const express = require("express")
const router = express.Router()
const ProductController = require("../controllers/ProductController")
const auth = require("../auth")

// Get all products
router.get("/", ProductController.getAllProducts)

// Get all active products
router.get("/active", ProductController.getAllActiveProducts)

// Get a single product
router.get("/:id", ProductController.getProduct)

// Create a product
router.post("/create", auth.verify, ProductController.createProduct)

// Update a product
router.patch("/:id/update", auth.verify, ProductController.updateProduct)

// Archive a product
router.patch("/:id/archive", auth.verify, ProductController.archiveProduct)

// Activate a product
router.patch("/:id/activate", auth.verify, ProductController.activateProduct)

module.exports = router