const mongoose = require("mongoose")

const user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	nickname: {
		type: String,
		required: [true, "Nickname is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
	{
		productId: {
			type: String,
			required: [true, "Product ID is required"]
		},
		orderedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Ordered"
		}
	}
	]

})

module.exports = mongoose.model("User", user_schema)