const User = require("../models/User")
const auth = require('../auth')
const bcrypt = require("bcrypt")
const Product = require("../models/Product")
const jwt = require("jsonwebtoken")

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true
		} 

		return false
	})
}

module.exports.registerUser = async (request_body) => {
	try {
		const existing_user = await User.findOne({ email: request_body.email });
		if (existing_user) {
			return {
				message: 'User already exists!',
			};
		} else {
			const hashedPassword = await bcrypt.hash(request_body.password, 10);
			let new_user = new User({
				firstName: request_body.firstName,
				lastName: request_body.lastName,
				email: request_body.email,
				mobileNo: request_body.mobileNo,
				nickname: request_body.nickname,
				password: hashedPassword,
			});
			const registered_user = await new_user.save();
			return { message: 'User successfully registered!' };
		}
	} catch (error) {
		return { error: error.message };
	}
};

module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result === null){
			return "The user doesn't exist."
		}

		
		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

		if(is_password_correct){
			return {
				
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return 'The email and password combination is not correct!'
	})
}

// For getting user details from the token
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}

// For creating an order(Checkout)

module.exports.createOrder = async (request_body) => {
	let userSaveStatus = await User.findById(request_body.user_id).then(user => {
		user.orders.push({productId: request_body.product_id})
		return user.save().then((user, error) => {	
			if(error){
				return false
			}
			// Returns true if the orders field has added a new product inside
			return true
		})
	})

	let productSaveStatus = await Product.findById(request_body.product_id).then(product => {
		product.customers.push({userId: request_body.user_id})

		return product.save().then((product, error) => {
			if (error){
				return false
			}

			return true
		})
	})

	// Checks if both User and Product model have been modified
	if (userSaveStatus && productSaveStatus){
		return {
			message: "You have ordered successfully"
		}
	   }else {
			return {
				message: "Something went wrong"
			}
	}
}

// Get all users (To view the user orders)
module.exports.getAllUsers = async (request, response) => {
	var decoded, authorization = request.headers.authorization.split(' ')[1]

	try {
		decoded = jwt.verify(authorization, process.env.JWT_SECRET);

	} catch (e){
		return response.status(401).send('unauthorized');
	}

	if (!decoded.isAdmin){
		return response.status(401).send({message: 'You are not authorized to view user details'})
	}

	const users = await User.find({}).then((result, error) => {
		if (error){
			return response.status(500).send
		}(error)
		return response.status(202).send({
			message: "Here are the users",
			data: result
		})
	})

}