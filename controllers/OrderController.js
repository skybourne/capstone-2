// const Product = require("../models/Product")
// const User = require("../models/User");
// const jwt = require("jsonwebtoken")
// const bcrypt = require("bcrypt")
// const Order = require("../models/Order")

// // Non-admin User checkout (Create Order)
// module.exports.createOrder = async (request, response) => {
//   const { userId, productId, quantity } = request.body;

//   try {
//     // Check if the user exists
//     const user = await User.findById(userId);
//     if (!user) {
//       return response.status(404).send({ message: "User not found" });
//     }

//     // Check if the product exists
//     const product = await Product.findById(productId);
//     if (!product) {
//       return response.status(404).send({ message: "Product not found" });
//     }

//     // Calculate total amount
//     const totalAmount = product.price * quantity;

//     // Create a new order
//     const newOrder = new Order({
//       userId,
//       productId,
//       quantity,
//       totalAmount,
//     });

//     await newOrder.save();
//     return response.status(201).send({
//       message: "Order created successfully",
//       data: newOrder,
//     });
//   } catch (error) {
//     return response.status(500).send({
//       message: "Error occurred while creating the order",
//       error: error.message,
//     });
//   }
// };


// Add to cart
// module.exports.addToCart = async (request, response) => {
// 	let cart = {
// 		products: [request.body.]
// 	}
// }