const Product = require("../models/Product")
const User = require("../models/User");
const jwt = require("jsonwebtoken")


//Get all products
module.exports.getAllProducts = (request, response) => {
    return Product.find({}).then((products, error) => {
        if (error){
            return error
        }

        return response.status(200).send({
            products: products
        });
    })
}

//Get all active products
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then((products, error) => {
		if (error){
			return error
		}

		return response.status(200).send({
			activeProducts: products
		})
	})
}

//Get a single product
module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.id).then((result, error) => {
		if (error){
			return error
		}
		return response.status(202).send(result)
	})
}


// Create a product
module.exports.createProduct = async (request, response) => {
  var decoded, authorization = request.headers.authorization.split(' ')[1]
  
  try {
    decoded = jwt.verify(authorization, process.env.JWT_SECRET);
  } catch (e) {
    return response.status(401).send('unauthorized');
  }
  
  if (!decoded.isAdmin) {
    return response.status(401).send({ message: 'You are not authorized to create a product' })
  }

  const new_product = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
      });

  await new_product.save().then(data => {
        return response.send({
            message:"Product created successfully",
            data: data
        });
    }).catch(err => {
        return response.status(500).send({
            message: err.message || "Some error occurred while creating product"
        });
    });
};

// Update Product Information
module.exports.updateProduct = async (request, response) => {
	var decoded, authorization = request.headers.authorization.split(' ')[1]
  
  try {
    decoded = jwt.verify(authorization, process.env.JWT_SECRET);
  } catch (e) {
    return response.status(401).send('unauthorized');
  }
  
  if (!decoded.isAdmin) {
    return response.status(401).send({ message: 'You are not authorized to update a product' })
  }

  let updated_product = {
  	name: request.body.name,
  	description: request.body.description,
  	price: request.body.price,
  	isActive: request.body.isActive

  }

  const result = await Product.findByIdAndUpdate(request.params.id, updated_product).then((result, error) => {
  	if (error){
  		return response.status(500).send
  	}(error)

  	return response.status(202).send({
          message: "Product updated successfully!",
          data: result
        })
  })
}

// Archive Product
module.exports.archiveProduct = (request, response) => {
  var decoded, authorization = request.headers.authorization.split(' ')[1]
  
  try {
    decoded = jwt.verify(authorization, process.env.JWT_SECRET);
  } catch (e) {
    return response.status(401).send('unauthorized');
  }
  
  if (!decoded.isAdmin) {
    return response.status(401).send({ message: 'You are not authorized to archive a product' })
  }

  return Product.findById(request.params.id).then((product, error) => {

    if (error){
      return response.status(500).send(error)

    }

    product.isActive = false

    return product.save().then((archived_product, error) => {
      if (error){
        return response.status(500).send(error)
      }

      return response.status(202).send({
        message: "Archived Product",
        data: archived_product
      })
    })
  })
}

// Activate Product
module.exports.activateProduct = (request, response) => {
  var decoded, authorization = request.headers.authorization.split(' ')[1]
  
  try {
    decoded = jwt.verify(authorization, process.env.JWT_SECRET);
  } catch (e) {
    return response.status(401).send('unauthorized');
  }
  
  if (!decoded.isAdmin) {
    return response.status(401).send({ message: 'You are not authorized to activate a product' })
  }

  return Product.findById(request.params.id).then((product, error) => {

    if (error){
      return response.status(500).send(error)

    }

    product.isActive = true

    return product.save().then((activated_product, error) => {
      if (error){
        return response.status(500).send(error)
      }

      return response.status(202).send({
        message: "Activated Product",
        data: activated_product
      })
    })
  })
}