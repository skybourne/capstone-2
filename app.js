// Setup dependencies
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config() // Initialize ENV
const user_routes = require("./routes/userRoutes")
const product_routes = require("./routes/productRoutes")
// const order_routes = require("./routes/orderRoutes")

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.qb1qgfz.mongodb.net/capstone-2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Connected to MongoDB!"))

// Server Setup
const app = express()

// Middleware
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Routes
app.use("/users", user_routes)
app.use("/products", product_routes)
// app.use("/orders", order_routes)

// Server Listening
// To dynamically switch from the port of the localhost to the port of the cloudserver, we have to use an OR operator to check which one express.js will use
app.listen(process.env.PORT || 3001, () => {
	console.log(`API is now running on port ${process.env.PORT || 3001}`)
})